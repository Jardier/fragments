package com.android.fragments.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.android.fragments.R

/**
 * A simple [Fragment] subclass.
 */
class ContatosFragment : Fragment() {

    lateinit var tvContado : TextView;

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_contatos, container, false);
        tvContado = view.findViewById(R.id.tvContato);
        tvContado.text = "Jardier B Aires";

        return view;
    }


}
