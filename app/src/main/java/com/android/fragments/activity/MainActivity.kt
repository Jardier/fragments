 package com.android.fragments.activity

import android.content.res.Resources
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import com.android.fragments.R
import com.android.fragments.fragments.ContatosFragment
import com.android.fragments.fragments.ConversasFragment
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*

 class MainActivity : AppCompatActivity() {

    lateinit var btnConversas : Button;
    lateinit var btnContatos : Button;
    lateinit var snackbar: Snackbar;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Removendo a sombra da ActionBar
        supportActionBar?.elevation = 0f;

        btnContatos = findViewById(R.id.bntContatos);
        btnConversas = findViewById(R.id.bntConversas);


        //Configurando o objetopara o Fragmento
        this.replaceFragment(ContatosFragment());

        //Criando uma ação no botão
        btnConversas.setOnClickListener(View.OnClickListener {
            replaceFragment(ConversasFragment());


            snackbar = Snackbar.make(it, "Confirmar", Snackbar.LENGTH_LONG);
            snackbar.setTextColor(resources.getColor(R.color.colorPrimary));
            snackbar.setDuration(Snackbar.LENGTH_LONG);
            snackbar.show();

        })

        //com lambda
        btnContatos.setOnClickListener(){
            v: View? -> replaceFragment(ContatosFragment());
        }

    }

    private fun AppCompatActivity.replaceFragment(fragment : Fragment) {
        val fragmentManger = supportFragmentManager;
        val transaction = fragmentManger.beginTransaction();
        transaction.replace(R.id.flResultado, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
